import {Component, Input} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss'],
})
export class ItemsComponent {
  @Input() id!: number;
  @Input() todo!: any;
  @Input() todoList!: any[];
  bool: boolean = false;
  constructor(private _snackBar: MatSnackBar) {}
  ngOnInit(){

  }
  delete(): void{
    console.log('1', this.todoList)
    setTimeout( (): void => {
      // @ts-ignore
      let find = this.todoList.find( x => x.id == this.id)
      const index: number = this.todoList.indexOf(find);
      if (index > -1) {
        this.todoList.splice(index, 1);
      }
      this.openSnackBar(find.value + ' supprimé')
    }, 1000)
  }

  openSnackBar(message: string
  ): void {
    this._snackBar.open(message, '', {
      duration: 1000
    });
  }
}
