import { Component } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {CdkDragDrop, moveItemInArray} from "@angular/cdk/drag-drop";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent {
  id: number = 0;
  todo: any[] = [];
  form: FormGroup = new FormGroup({
    id: new FormControl(),
    value: new FormControl()
  });
  ngOnInit():void{
    if(localStorage.getItem('todo')){
      // @ts-ignore
      this.todo = JSON.parse(localStorage.getItem('todo'))
    }
  }

  drop(event: CdkDragDrop<string[]>): void {
    moveItemInArray(this.todo, event.previousIndex, event.currentIndex);
  }
  submit(): void{
    this.id += 1;
    this.form.setValue({
      id: this.id,
      value: this.form.value.value
    })
    this.todo.push(this.form.value)
    this.form.setValue({
      id: 0,
      value: ""
    })
    localStorage.setItem('todo', JSON.stringify(this.todo))

  }

}
