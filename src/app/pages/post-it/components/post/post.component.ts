import {Component, Input} from '@angular/core';
import {PostModel} from "../../../../models/post/post.model";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent {
  @Input() postit!: PostModel;
  @Input() postitList!: PostModel[];
  constructor(private _snackBar: MatSnackBar) {}
  delete(): void{
      // @ts-ignore
      let find: PostModel | undefined = this.postitList.find( (x: number): boolean => x.id == this.postit.id)
      if (find) {
        const index: number = this.postitList.indexOf(find);
        if (index > -1) {
          this.postitList.splice(index, 1);
        }
        this.openSnackBar(find.title + ' supprimé')
        localStorage.setItem('postit', JSON.stringify(this.postitList))
      }
  }
  openSnackBar(message: string
  ): void {
    this._snackBar.open(message, '', {
      duration: 1000
    });
  }

}
