import { Component } from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {DialogComponent} from "./components/dialog/dialog.component";
import {PostModel} from "../../models/post/post.model";

@Component({
  selector: 'app-post-it',
  templateUrl: './post-it.component.html',
  styleUrls: ['./post-it.component.scss']
})
export class PostItComponent {
  title: string = 'matDialog';
  id: number = 0;
  dataFromDialog: any;
  postItList!: PostModel[];


  constructor(public dialog: MatDialog) {}
  ngOnInit(): void{
    if(localStorage.getItem('postit')){
      // @ts-ignore
      this.postItList = JSON.parse(localStorage.getItem('postit'))
    }else{
      this.postItList = []
    }

  }
  random_color(): string {
    let colors: string[] = ['#fff740', '#feff9c','#7afcff','#ff65a3','#ff7eb9']
    return colors[Math.floor(Math.random()*colors.length)];
  }
  showPrompt(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '350px',
      height: '350px',
    });


    dialogRef.afterClosed().subscribe((data): void => {
      this.dataFromDialog = data.form;
      if (data.clicked === 'submit') {
        this.id += 1;
        let postIt: PostModel = {
          id: this.id,
          title: this.dataFromDialog.title,
          content: this.dataFromDialog.content,
          date: new Date(),
          color: this.random_color()
        }
      this.postItList.push(postIt)
      localStorage.setItem('postit', JSON.stringify(this.postItList))
      }
    });
  }
}
