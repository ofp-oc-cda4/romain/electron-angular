import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {TodoComponent} from "./pages/todo/todo.component";
import {PostItComponent} from "./pages/post-it/post-it.component";

const routes: Routes = [
  {path: 'todo', component: TodoComponent},
  {path: 'postit', component: PostItComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
