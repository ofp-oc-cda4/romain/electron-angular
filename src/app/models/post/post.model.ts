export interface PostModel{
  id: number;
  title: string;
  content: string;
  date: Date;
  color: string;
}
