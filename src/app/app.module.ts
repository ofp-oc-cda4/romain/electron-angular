import {LOCALE_ID, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TodoComponent } from './pages/todo/todo.component';
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatCardModule} from "@angular/material/card";
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { ItemsComponent } from './pages/todo/components/items/items.component';
import {MatCheckboxModule} from "@angular/material/checkbox";
import { PostItComponent } from './pages/post-it/post-it.component';
import {MatBadgeModule} from "@angular/material/badge";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import { PostComponent } from './pages/post-it/components/post/post.component';
import {MatDialogModule} from "@angular/material/dialog";
import { DialogComponent } from './pages/post-it/components/dialog/dialog.component';
import {CdkDrag, CdkDropList} from "@angular/cdk/drag-drop";
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

registerLocaleData(localeFr, 'fr');
@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    ItemsComponent,
    PostItComponent,
    PostComponent,
    DialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatCardModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    FormsModule,
    MatBadgeModule,
    MatSnackBarModule,
    MatDialogModule,
    CdkDrag,
    CdkDropList
  ],
  providers: [
    MatSnackBarModule,
    {provide: LOCALE_ID, useValue: 'fr' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
